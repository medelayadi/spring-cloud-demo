package org.mel.ConfigClient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mohamed on 22/03/2018.
 */
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RefreshScope
    @RestController
    class MessageRestController {

        @Value("${author:Hello default}")
        private String message;

        @RequestMapping("/author")
        String getMessage() {
            return this.message;
        }
    }
}
