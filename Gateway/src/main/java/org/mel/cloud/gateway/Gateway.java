package org.mel.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * Created by Mohamed on 24/03/2018.
 */
@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class Gateway {
    public static void main(String...strings) {
        SpringApplication.run(Gateway.class, strings);
    }
}
