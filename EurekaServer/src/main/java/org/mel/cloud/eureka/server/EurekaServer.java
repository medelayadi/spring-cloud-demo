package org.mel.cloud.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by Mohamed on 24/03/2018.
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer {
    public static void main(String...strings) {
        SpringApplication.run(EurekaServer.class, strings);
    }
}
