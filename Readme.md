# Spring cloud sample demo

This repo contains simple and basic implementations of spring cloud different aspects.
 
Up until now, it contains following projects:
-
- Embeded config repository.
- `Config Server`: contains configuration for other projects .
- `Eureka Discovery`: used to register all the system components.
- `Gateway`: reverse proxy to our system components.
- Including `data-jpa` with an `h2-database` (and maybe later `postgres`)

Later, many `features` will be added. Roadmap: 
- 
- :warning: They are not implemented yet :)
- Implement caching
- Add other microservices to establish communication
- Include pagination & sorting
- Implement front with `reactjs` after creating some dumb services
- Implement SSL
- Logs management (`elk` stack)
- Introduce `docker` (lot of fun here..)
- Ci & cd jobs
- Implement `oauth` and security
- Other front implementations

Developer Comments & how to use
-
With current configuration, start with this order:

`Config server -> Eureka -> Gateway -> DemoService`

To test the demo service, either you access it directly or you pass via gateway

- directly: `http://localhost:9090/samples`
- directly: `http://localhost:8080/demoservice/samples`