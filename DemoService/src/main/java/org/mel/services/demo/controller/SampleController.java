package org.mel.services.demo.controller;

import org.mel.services.demo.Repository.SampleRepository;
import org.mel.services.demo.controller.constants.Constants;
import org.mel.services.demo.models.Sample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Mohamed on 23/03/2018.
 */
@RestController
public class SampleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SampleController.class);

    @Autowired
    SampleRepository sampleRepository;
    //region DEMO
    @Value("${author:MEL}")
    private String author;
    @Value("${author2}")
    private String author2;

    @GetMapping(path = Constants.SAMPLE_HELLO_ENDPOINT)
    public ResponseEntity<String> sayHello(@PathVariable("name") final String name) {
        return buildResEntFromStruind(HttpStatus.OK, "hello " + name + ", greetings from " + this.author2);
    }
    @GetMapping(path = Constants.SAMPLE_AUTHOR_ENDPOINT)
    public ResponseEntity<String> getAuthor2() {
        return buildResEntFromStruind(HttpStatus.OK, this.author2);
    }

    private static ResponseEntity<String> buildResEntFromStruind(final HttpStatus status, final String text) {
        return new ResponseEntity<String>(text, status);
    }
    //endregion

    @GetMapping(Constants.SAMPLE_SAMPLES_ENDPOINT)
    public ResponseEntity<List<Sample>> getAllSamples() {
        try {
            final Iterable<Sample> allSamples = sampleRepository.findAll();
            final List<Sample> out = new ArrayList<>();
            allSamples.iterator().forEachRemaining(out::add);
            return new ResponseEntity<List<Sample>>(out, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.debug(e.getLocalizedMessage(), e);
            LOGGER.info(e.getLocalizedMessage());
            return new ResponseEntity<List<Sample>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(Constants.SAMPLE_SAMPLES_ENDPOINT)
    public ResponseEntity<Sample> saveSample(@Valid @RequestBody final Sample sample) {
        try {
            return new ResponseEntity<Sample>(sampleRepository.save(sample), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.debug(e.getLocalizedMessage(), e);
            LOGGER.info(e.getLocalizedMessage());
            return new ResponseEntity<Sample>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping(Constants.SAMPLE_SAMPLES_ID_ENDPOINT)
    public ResponseEntity<Sample> update(
            @PathVariable("id") final Long id,
            @Valid @RequestBody final Sample sample)
    {
        try {
            final Optional<Sample> oldSample = sampleRepository.findById(id);
            if(oldSample.isPresent()) {
                sample.setCreatedAt(oldSample.get().getCreatedAt());
            }
            sample.setId(id);
            return new ResponseEntity<Sample>(sampleRepository.save(sample), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.debug(e.getLocalizedMessage(), e);
            LOGGER.info(e.getLocalizedMessage());
            return new ResponseEntity<Sample>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(Constants.SAMPLE_SAMPLES_ID_ENDPOINT)
    public ResponseEntity<Sample> delete(@PathVariable("id") final Long id)
    {
        try {
            final Sample sample = new Sample();
            sample.setId(id);
            sampleRepository.delete(sample);
            return new ResponseEntity<Sample>(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.debug(e.getLocalizedMessage(), e);
            LOGGER.info(e.getLocalizedMessage());
            return new ResponseEntity<Sample>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
