package org.mel.services.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Created by Mohamed on 23/03/2018.
 */
@SpringBootApplication
@EnableEurekaClient
public class DemoService {
    public static void main(String...strings) {
        SpringApplication.run(DemoService.class, strings);
    }
}
