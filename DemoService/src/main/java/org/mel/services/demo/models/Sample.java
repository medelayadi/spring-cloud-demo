package org.mel.services.demo.models;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Mohamed on 27/03/2018.
 */
@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "sample")
@EntityListeners({AuditingEntityListener.class})
public class Sample implements Serializable {

    @Id @GeneratedValue private long id;
    @Column(name = "description") private String desc;
    @Column(name = "commentaire") private String comment;
    @CreatedDate @Column(updatable = false) private Date createdAt;
    @LastModifiedDate private Date updatedAt;

    @PrePersist
    public void setCreatedAtDate() {
        this.createdAt = new Date();
    }
    @PreUpdate
    public void setUpdatedAtDate() {
        this.updatedAt = new Date();
    }
    @Override
    public String toString() {
        return String.format("Sample[id=%d, desc='%s', comment='%s', lastModified='%s']", id, desc, comment, updatedAt);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
