package org.mel.services.demo.Repository;

import org.mel.services.demo.models.Sample;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Mohamed on 27/03/2018.
 */
public interface SampleRepository extends CrudRepository<Sample, Long> {
}
