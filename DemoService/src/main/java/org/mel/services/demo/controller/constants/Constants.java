package org.mel.services.demo.controller.constants;

/**
 * Created by Mohamed on 26/03/2018.
 */
public class Constants {
    public static final String SAMPLE_AUTHOR_ENDPOINT = "/author2";
    public static final String SAMPLE_HELLO_ENDPOINT = "/hello/{name}";
    public static final String SAMPLE_SAMPLES_ENDPOINT = "/samples";
    public static final String SAMPLE_SAMPLES_ID_ENDPOINT = "/samples/{id}";
}
