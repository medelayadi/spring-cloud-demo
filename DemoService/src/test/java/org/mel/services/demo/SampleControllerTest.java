package org.mel.services.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mel.services.demo.controller.constants.Constants;
import org.mel.services.demo.controller.SampleController;
import org.mockito.BDDMockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.hamcrest.core.Is;

/**
 * Created by Mohamed on 26/03/2018.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(SampleController.class)
@ActiveProfiles("test")
public class SampleControllerTest {
    @Autowired private MockMvc mvc;
    @MockBean private SampleController sampleController;

    @Value("${author2}") private String author2;
    @Value("${author}") private String author;

    @Test public void shouldReturnAuthorPropertyOnAuthor2Endpoint() throws Exception {
        //given
        BDDMockito.given(sampleController.getAuthor2())
                .willReturn(new ResponseEntity<String>(this.author2, HttpStatus.OK));
        //then
        mvc.perform(MockMvcRequestBuilders.get(Constants.SAMPLE_AUTHOR_ENDPOINT))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Is.is(author2)));
    }

    @Test public void shouldSayHelloToCorrectName() throws Exception {
        //given
        BDDMockito.given(sampleController.sayHello(BDDMockito.anyString()))
                .willAnswer((new Answer<Object>() {
                    @Override
                    public ResponseEntity<String> answer(InvocationOnMock invocation) throws Throwable {
                        Object[] args = invocation.getArguments();
                        return new ResponseEntity<String> ( "hello " + (String) args[0] + ", greetings from " + author2, HttpStatus.OK);
                    }
                }));
        //when
        final String name = "MEL";
        //then
        mvc.perform(MockMvcRequestBuilders.get(Constants.SAMPLE_HELLO_ENDPOINT, name))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Is.is( "hello " + name + ", greetings from " + author2)));
    }
}
