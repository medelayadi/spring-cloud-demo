package org.mel.ConfigServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * Created by Mohamed on 22/03/2018.
 */
@EnableConfigServer
@SpringBootApplication
public class ConfigServer {

    public static void main(String...strings) {
        SpringApplication.run(ConfigServer.class, strings);
    }
}
